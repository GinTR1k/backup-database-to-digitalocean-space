#!/bin/bash
SERVERID=$1
SPACENAME=$2

MYSQLUSER=$3
MYSQLPASSWORD=$4

FILEDATETIME=`date +%d%m%y-%H-%M-%S-%Z`
FOLDERDATETIME=`date +%d%m%y`

SRC='/.database.sql'
DST='s3://'$SPACENAME'/backups/database/'$SERVERID'/'

GIVENNAME=$SERVERID'_database'
TARNAME=$GIVENNAME"_"$FILEDATETIME".tar.gz"

backupDB(){
    echo "\n##### Dumping DataBase #####"
    if /usr/bin/mysqldump --user=$MYSQLUSER --password=$MYSQLPASSWORD -A > $SRC; then
	echo "##### Done dumping DataBase #####"
        return 0
    else
	echo "##### Failed to dump DataBase #####"
	return 1
    fi
}
tarandzip(){
    echo "\n##### Gathering files #####"
    if tar -czvf /$TARNAME $SRC; then
        echo "##### Done gathering files #####"
        return 0
    else
        echo "##### Failed to gather files #####"
        return 1
    fi
}
moveToSpace(){
    echo "\n##### Moving backups to space #####"
    if s3cmd put /$TARNAME $DST$FOLDERDATETIME'/'; then
        echo "##### Done moving files to "$DST$FOLDERDATETIME" #####"
        return 0
    else
        echo "##### Failed to move files to the Space #####"
        return 1
    fi
}
clearSentData(){
    rm -rf /$TARNAME $SRC
}

showhelp(){
        echo "\n\n############################################"
        echo "# backup.sh                                #"
        echo "############################################"
        echo "\nThis script will backup files/folders into a single compressed file and will store it in the current folder."
        echo "In order to work, this script needs the following three parameters in the listed order: "
        echo "\t- The unique identifier of this machine."
        echo "\t- The name of the Space where you want to store the backup at."
        echo "\t- The username of MySQL BACKUPUSER."
        echo "\t- The password of MySQL BACKUPUSER.\n"
        echo "Example: bash backup.sh Server001 testSpace BACKUPUSER 'SuperPa$$word'\n"
}

if [ ! -z "$SERVERID" ]; then
	if backupDB; then
		if tarandzip; then
			moveToSpace
		fi
		clearSentData
	fi
else
	showhelp
fi
